<?php
    include '../config.php';
    $conn = mysqli_connect($host, $username, $password , $database);
    $idVideo = '';
    if(isset($_GET["id"])) $idVideo = $_GET["id"];
    $data = '';
    if($idVideo){
        $data = mysqli_query($conn,"SELECT * FROM videos WHERE video_id ='$idVideo'")->fetch_object();
    }

    $dataSource = json_encode($res['data']); 
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Youtube Monitaz</title>
        <link rel="stylesheet" href="../assets/css/style-detail.css"> 
        <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
        <script type="text/javascript" src="../assets/js/custom.js"></script>  
    </head>
    <body>
        <div class="container">
            <div class="col-md-12 box-default">
                <?php if($data):?>
                    <div class="panel panel-default">
                        <div class="panel-body">
                        <section class="post-heading">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="media">
                                            <div class="table-responsive lstproducts">
                                            <table class="table" style="font-size:12px;">
                                                <thead>
                                                <tr>
                                                    <th class="text-left">Kênh truyền thông</th>
                                                    <th class="text-left"><a href="https://www.youtube.com/channel/<?php echo $data->channel_id ?>" target="_blank" class="anchor-username"><h4 style="font-size:12px;"><?php echo $data->channel_name ?></h4></a></th>
                                                    <th class="text-left">Hình thức</th>
                                                    <th class="text-left"></th>
                                                </tr>
                                                <tr>
                                                    <th class="text-left">Ngày phát hành:</th>
                                                    <th class="text-left"><a href="https://www.youtube.com/watch?v=<?php echo $data->video_id ?>" target="_blank" class="anchor-username"><h4 style="font-size:12px;"><?php echo $data->publishedAt ?></th>
                                                    <th class="text-left">Phương tiện:</th>
                                                    <th class="text-left"></th>
                                                </tr>
                                                <tr>
                                                    <th class="text-left">Trang:</th>
                                                    <th class="text-left"><a href="https://www.youtube.com/channel<?php echo $data->channel_id ?>" target="_blank" class="anchor-username"><h4 style="font-size:12px;"><?php echo $data->channel_name ?></h4></a></th>
                                                    <th class="text-left">Chuyên mục:</th>
                                                    <th class="text-left"></th>
                                                </tr>
                                                <tr>
                                                    <th class="text-left">Kích thước:</th>
                                                    <th class="text-left"></th>
                                                    <th class="text-left">Chi phí:</th>
                                                    <th class="text-left"></th>
                                                </tr>
                                                <tr>
                                                    <th class="text-left">Tiêu đề:</th>
                                                    <th class="text-left" colspan="3"><a href="https://www.youtube.com/watch?v=<?php echo $data->video_id ?>" target="_blank" class="anchor-username"><h4 style="font-size:12px;"><?php echo $data->title ?></h4></a></th>
                                                </tr>
                                                <tr>
                                                    <th class="text-left">Link:</th>
                                                    <th class="text-left" colspan="3"><a href="https://www.youtube.com/watch?v=<?php echo $data->video_id ?>" target="_blank" class="anchor-username"><h4 style="font-size:12px;">https://www.youtube.com/watch?v=<?php echo $data->video_id ?></h4></a></th>
                                                </tr>
                                                </thead>
                                            </table>
                                            </div>
                                        
                                        
                                        <div class="media-body">
                                            <a href="https://www.youtube.com/watch?v=<?php echo $data->video_id ?>" target="_blank" class="anchor-username"><h4 style="font-size:15px; color: #F00; text-align:center; text-transform:uppercase;"><?php echo $data->title ?></h4></a>
                                            <a href="https://www.youtube.com/watch?v=<?php echo $data->video_id ?>" target="_blank" class="anchor-time">Đăng ngày: <?php echo $data->publishedAt ?></a>
                                        </div>
                                        </div>
                                    </div>
                                </div>             
                        </section>
                        <section class="post-body" style="padding-top:10px;">
                            <img class="img-video" src="https://img.youtube.com/vi/<?php echo $data->video_id?>/maxresdefault.jpg">
                            <hr>
                            <p><?php echo $data->description ?></p>
                        </section>
                        <section class="post-footer">
                            <hr>
                            
                        </section>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="panel panel-default empty-content">
                        <p class="empty">Không tồn tại Video ID này!</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </body>
</html>