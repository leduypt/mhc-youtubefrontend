<?php
    date_default_timezone_set('Asia/Ho_Chi_Minh');  
    include 'config.php';
    $conn = mysqli_connect($host, $username, $password , $database);
    $message = $video = $parent =  '';
    $timeAlert = date('d/m/Y H:i:s');
    if(isset($_POST["company"])) $company = $_POST["company"];
    if(isset($_POST["message"])) $message = $_POST["message"];
    if(isset($_POST["video"])) $video = $_POST["video"];
    if($message && $video):
        try {
            if ($result = mysqli_query($conn, "SELECT * FROM telegrams WHERE company_id = $company")) {
                // Fetch one and one row
                while ($row = mysqli_fetch_row($result)) {
                    $apiToken = $row[1];
                    $groupID = $row[2];
                }
                mysqli_free_result($result);
            }

            echo json_encode($result);

            
            if($apiToken && $groupID):
                $dataInsert = mysqli_query($conn, "INSERT INTO logs (video_id, alert_time) VALUES ('$video', '$timeAlert')");
                $data = [
                    'chat_id' => $groupID,
                    'text' => $message,
                    'parse_mode' => 'html'
                ];
                $response = file_get_contents("https://api.telegram.org/bot$apiToken/sendMessage?" . http_build_query($data) );
                $res = [
                    'status' => 'success',
                    'message' => 'Alert Success'
                ]; 
                echo json_encode($res);
            else:
                $res = [
                    'status' => 'error',
                    'message' => 'Không lấy được apiToken và Group ID Telegram'
                ]; 
                echo json_encode($res); 
            endif;

        } catch (\Throwable $th) {
            $res = [
                'status' => 'error',
                'message' => $th
            ]; 
            echo json_encode($res); 
        }

    else:
        $res = [
            'status' => 'error',
            'message' => 'Alert Fail'
        ]; 
        echo json_encode($res); 
    endif;