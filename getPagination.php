<?php
    include 'config.php';
    $conn = mysqli_connect($host, $username, $password , $database);
    $page = 1;
    $limit = 50;

    $query = "SELECT * FROM videos, companys WHERE companys.id = videos.company_id";
    if(isset($_POST["alert"]) && $_POST["alert"] != 2) $query .= " && alert_status=".$_POST['alert'];
    if(isset($_POST["parent"]) && $_POST["alert"]) $query .= " && parent=".$_POST['parent'];
    if(isset($_POST["type"]) && $_POST["type"] != 2) $query .= " && type='".$_POST['type']."'";
    if(isset($_POST["status"]) && $_POST["status"] != 2) $query .= " && status=".$_POST['status'];
    if(isset($_POST["company"]) && $_POST["company"] != 'all') $query .= " && company_id=".$_POST['company']; 
    if(isset($_POST["settlement"]) && $_POST["settlement"] != 'all') $query .= " && settlement='".$_POST['settlement']."'";
    if(isset($_POST["fromDate"]) && isset($_POST["toDate"]) && $_POST["fromDate"] && $_POST["toDate"]) $query .= " && publishedAt BETWEEN '".$_POST["fromDate"]." 00:00:00' AND '".$_POST["toDate"]." 00:00:00'";
    if(isset($_POST["keyword"]) && $_POST["keyword"]) $query .= " && title LIKE '%".$_POST['keyword']."%'";
    if(isset($_POST["page"]) && $_POST["alert"]) $page = $_POST['page'];

    $result = $conn->query($query);
    $total_record = mysqli_num_rows($result);
    $total_page=ceil($total_record/$limit);

    if($page < 1) $page=1;
    if($page > $total_page) $page = $total_page;
    $start = ($page-1) * $limit;
    $query .= " limit $start,$limit";
    $data = $conn->query($query);

    $html = '';
    $disablePrev = $page == 1 ? "disabled" : "";
    $disableNext = $page == $total_page ? "disabled" : "";

    $html .= ' <ul class="pagination-list" total-page="'.$total_page.'">';
        for($i=1;$i<=$total_page;$i++):
            $current = $page == $i ? "is-current" : "";
            $html .= '<li class="'.$current.'"><a href="javascript:void(0)" page-number="'.$i.'" class="pagination-link '.$current.'" aria-label="Goto page '.$i.'">'.$i.'</a></li>';
        endfor;
    $html .= '</ul>';

    echo $html;