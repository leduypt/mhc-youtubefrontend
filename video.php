<?php 
    include 'config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MH+ Youtube</title>
        <link rel="stylesheet" href="../assets/css/bulma.min.css">
        <link rel="stylesheet" href="../assets/css/style.css"> 
        <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet" />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script type="text/javascript" src="../assets/js/custom.js"></script>  
    </head>
    <body>
        <?php

            $page = 1;
            $limit = 50;
            $idVideo = '';
            if(isset($_GET["id"])) $idVideo = $_GET["id"];
            $conn = mysqli_connect($host, $username, $password , $database);
            mysqli_set_charset($conn,"utf8");
                /* check connection */
            if (mysqli_connect_errno()) {
                printf("MySQL connecttion failed: %s", mysqli_connect_error());
            } else {
                /* print server version */
                // printf("MySQL Server %s", mysqli_get_server_info($conn));
            }

            $sql = "SELECT * FROM videos WHERE parent='$idVideo' AND videos.remove=0";
            $result = $conn->query($sql);

            $sqlCompany = "SELECT * FROM companys";
            $resultCompany = $conn->query($sqlCompany);

            $total_record = mysqli_num_rows($result);
            
            $total_page=ceil($total_record/$limit);

            if(isset($_POST["page"]))
                $page=$_POST["page"];
            if($page<1) $page=1;
            if($page>$total_page) $page=$total_page;

            $start=($page-1)*$limit;

            $data = mysqli_query($conn,"
                SELECT * FROM companys, videos WHERE companys.id = videos.company_id && parent='$idVideo' && remove=0 limit $start,$limit
            ");
            /* close connection */
            mysqli_close($conn);
        ?>
        <section class="hero is-medium is-primary is-bold">
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">
                        MH+ Youtube
                    </h1>
                    <h2 class="subtitle">
                        Your local development environment
                    </h2>
                </div>
            </div>
        </section>
        <section class="section mhc-video-container">
            <div class="container mhc-filter">
                <div class="columns">
                    <div class="column field is-6">
                        <p class="control has-icons-left has-icons-right">
                            <input class="input" type="text" placeholder="Từ khoá" id="keyword" name="keyword">
                            <span class="icon is-small is-left">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            </span>
                        </p>
                    </div>
                    <div class="column field is-3">
                        <p class="control has-icons-left has-icons-right">
                            <input class="input" type="email" placeholder="Từ ngày" id="from_date" name="from_date">
                            <span class="icon is-small is-left">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                        </p>
                    </div>
                    <div class="column field is-3">
                        <p class="control has-icons-left has-icons-right">
                            <input class="input" type="email" placeholder="Đến ngày" id="to_date" name="to_date">
                            <span class="icon is-small is-left">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                        </p>
                    </div>
                </div>
                <div class="columns">
                    <div class="column field is-3 settlement-filter">
                        <div class="control has-icons-left">
                            <div class="select">
                                <select>
                                    <option value="all">All Settlement</option>
                                    <option value="none">None</option>
                                    <option value="negative">Negative</option>
                                    <option value="positive">Positive</option>
                                    <option value="neutral">Neutral</option>
                                </select>
                            </div>
                            <div class="icon is-small is-left">
                                <i class="fa fa-smile-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="column field is-3 company-filter">
                        <div class="control has-icons-left">
                            <div class="select">
                                <select>
                                    <option value="all">All Company</option>
                                    <?php while ($rowCompany = $resultCompany->fetch_assoc()): ?>
                                            <option value="<?php echo $rowCompany['id'] ?>"><?php echo $rowCompany['company_name'] ?></option>; 
                                    <?php endwhile; ?>
                                </select>
                            </div>
                            <div class="icon is-small is-left">
                                <i class="fa fa-smile-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div> -->
                    <div class="column field is-3 alert-filter">
                        <div class="control has-icons-left">
                            <div class="select">
                                <select>
                                    <option value="2">All Status Alert</option>
                                    <option value="0">Not Alert</option>
                                    <option value="1">Alert</option>
                                </select>
                            </div>
                            <div class="icon is-small is-left">
                                <i class="fa fa-bell-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="column field is-3 status-filter">
                        <div class="control has-icons-left">
                            <div class="select">
                                <select>
                                    <option value="2">All Status Check</option>
                                    <option value="0">Reject</option>
                                    <option value="1">Approved</option>
                                </select>
                            </div>
                            <div class="icon is-small is-left">
                                <i class="fa fa-bell-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="column field is-3 btn-filter">
                        <button class="button is-primary is-fullwidth">Filter</button>
                    </div>
                </div>
                <div class="columns">
                    <!-- <div class="column field is-3 type-filter">
                        <div class="control has-icons-left">
                            <div class="select">
                                <select>
                                    <option value="2">All Type</option>
                                    <option value="post">Post</option>
                                    <option value="comment">Comment</option>
                                </select>
                            </div>
                            <div class="icon is-small is-left">
                                <i class="fa fa-bell-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="parent_id hide"><?php echo $idVideo; ?></div>
            <div id="content-load" class="container">
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                        <tr class="has-background-primary">
                            <th>ID</th>
                            <th>Title</th>
                            <th>Parent Video</th>
                            <th>Content</th>
                            <!-- <th>Tags</th> -->
                            <th>Keyword</th>
                            <th>Published</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if ($result->num_rows > 0):
                            // output data of each row
                            while($row = $data->fetch_assoc()):
                        ?>
                            <tr>
                                <td class="mhc-videoid"><?php echo $row["id"]; ?></td>
                                <td class="title-video">
                                    <?php if($row["type"] == 'post'): ?>
                                        <a href="https://www.youtube.com/watch?v=<?php echo $row["video_id"]; ?>" target="_blank"><?php echo $row["title"]; ?></a>
                                    <?php else: ?>
                                        <a href="https://www.youtube.com/watch?v=<?php echo $row["video_id"]; ?>&lc=<?php echo $row["comment_id"]; ?>" target="_blank"><?php echo $row["title"]; ?></a>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if($row["parent"]): ?>
                                        <a href="https://www.youtube.com/watch?v=<?php echo $row["parent"]; ?>" target="_blank"><?php echo $row["parent"]; ?></a>
                                    <?php endif; ?>
                                </td>
                                <td class="description"><?php echo $row["description"]; ?></td>
                                <!-- <td>
                                    <div class="tags">
                                        <?php $tags = explode(',', $row["tags"]);
                                        foreach ($tags as $tag): ?>
                                            <span class="tag is-success is-lowercase"> <?php echo $tag ?></span>
                                        <?php endforeach; ?>
                                    </div>
                                </td> -->
                                <td class="keyword">
                                    <?php echo $row["keyword"]; ?></br>
                                    <button class="button is-danger is-light is-small is-fullwidth"><?php echo $row["company_name"]; ?></button>
                                </td>
                                <td>
                                    <?php 
                                    $date = new DateTime($row["publishedAt"]);
                                        echo $date->format('H:i:s d/m/Y');
                                    ?>
                                </td>
                                <td class="action-btn">
                                    <?php
                                        $alertStatus = $row["alert_status"] == '1' ? 'alert is-warning' : 'no-alert';
                                        $disableAlert = $row["alert_status"] == '1' ? 'disabled' : '';
                                    ?>
                                    <div class="buttons are-small mhc-action">
                                        <div class="company_id hide"><?php echo $row["company_id"] ?></div>
                                        <a href="/detail/index.php?id=<?php echo $row["video_id"]; ?>" class="button is-primary is-small is-rounded btn-view" target="_blank">View</a>
                                        <!-- <a href="/video.php?id=<?php echo $row["video_id"]; ?>" class="button is-primary is-small is-rounded btn-view" target="_blank">View Detail</a> -->
                                        <!-- <button class="button is-primary is-small is-rounded btn-view"><i class="fa fa-eye" aria-hidden="true"></i> View</button> -->
                                        <button class="button is-rounded is-small alert-btn <?php echo $alertStatus; ?>" <?php echo $disableAlert; ?>><i class="fa fa-bell-o" aria-hidden="true"></i> Alert</button>
                                    </div>
                                    <div class="checkStatus">
                                        <div class="control has-icons-left">
                                            <div class="select is-small is-rounded">
                                                <select>
                                                    <option value="0" <?php echo $row['status'] == '0' ? 'selected' : '' ?>>Đã check</option>
                                                    <option value="1" <?php echo $row['status'] == '1' ? 'selected' : '' ?>>Chưa check</option>
                                                </select>
                                            </div>
                                            <div class="icon is-small is-left">
                                                <i class="fa fa-smile-o" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="settlement">
                                        <div class="control has-icons-left">
                                            <div class="select is-small is-rounded">
                                                <select>
                                                    <option value="none" <?php echo $row['settlement'] == 'none' ? 'selected' : '' ?>>None</option>
                                                    <option value="negative" <?php echo $row['settlement'] == 'negative' ? 'selected' : '' ?>>Negative</option>
                                                    <option value="positive" <?php echo $row['settlement'] == 'positive' ? 'selected' : '' ?>>Positive</option>
                                                    <option value="neutral" <?php echo $row['settlement'] == 'neutral' ? 'selected' : '' ?>>Neutral</option>
                                                </select>
                                            </div>
                                            <div class="icon is-small is-left">
                                                <i class="fa fa-smile-o" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endwhile; endif;
                        ?>
                    </tbody>
                </table>
                <nav class="pagination is-rounded is-centered" role="navigation" aria-label="pagination">
                    <?php 
                        $disablePrev = $page == 1 ? "disabled" : "";
                        $disableNext = $page == $total_page ? "disabled" : "";
                    ?>
                    <!-- <a class="pagination-previous" <?php echo $disablePrev ?>><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Previous</a> -->
                    <ul class="pagination-list" total-page="<?php echo $total_page ?>">
                        <?php for($i=1;$i<=$total_page;$i++): ?>
                            <li class="<?php if($page == $i) echo "is-current"; ?>"><a href="javascript:void(0)" page-number="<?php echo $i; ?>" class="pagination-link <?php if($page == $i) echo "is-current"; ?> " aria-label="Goto page <?php echo $i; ?>"><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                    <!-- <a class="pagination-next" <?php echo $disableNext ?>>Next page <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a> -->
                </nav>
            </div>
        </div>
        <div id="modal-view" class="modal">
            <div class="modal-background"></div>
            <div class="modal-card">
                <header class="modal-card-head">
                <p class="modal-card-title">Video Detail</p>
                <button class="delete" aria-label="close"></button>
                </header>
                <section class="modal-card-body hcm-view-detail">
                <!-- Content ... -->
                </section>
                <footer class="modal-card-foot">
                <button class="button is-rounded is-small close-btn"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
                </footer>
            </div>
        </div>
        <div class="bkg-loader">
            <div class="loader"></div>
        </div>
    </body>
</html>