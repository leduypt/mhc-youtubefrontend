<?php
    include 'config.php';
    $conn = mysqli_connect($host, $username, $password , $database);
    $idVideo = $status = '';
    if(isset($_POST["video"])) $idVideo = $_POST["video"];
    if(isset($_POST["status"])) $status = $_POST["status"];
    $arrStatus = ['none', 'negative', 'positive', 'neutral'];
    if($idVideo != '' && $status != '' && in_array($status, $arrStatus)){
        $data = mysqli_query($conn,"SELECT settlement FROM videos WHERE id = $idVideo")->fetch_object();
        if($status != $data->settlement){
            $dataUpdate = mysqli_query($conn, "UPDATE videos SET settlement = '$status' WHERE id = $idVideo");
            if($dataUpdate)
                $res = [ 'status' => 'success', 'message' => 'Cập nhật thành công', 'data' => $status];
            else 
                $res = [ 'status' => 'error', 'message' => 'Cập nhật không thành công, Lỗi Update'];
            echo json_encode($res);
        }else{
            $res = ['status' => 'error', 'message' => 'Cập nhật không thành công, Trùng dữ liệu'];
            echo json_encode($res); 
        }
    }else{
        $res = ['status' => 'error', 'message' => 'Cập nhật không thành công, Sai dữ liệu đầu vào'];
        echo json_encode($res);  
    }