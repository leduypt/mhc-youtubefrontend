<?php
    include 'config.php';
    $conn = mysqli_connect($host, $username, $password , $database);
    $idVideo = $status = '';
    if(isset($_GET["video"])) $idVideo = $_GET["video"];
    if(isset($_GET["status"])) $status = $_GET["status"];
    $arrStatus = ['0', '1'];
    if($idVideo != '' && $status != '' && in_array($status, $arrStatus)){
        $data = mysqli_query($conn,"SELECT alert_status FROM videos WHERE id = $idVideo")->fetch_object();
        if($status != $data->alert_status){
            $dataUpdate = mysqli_query($conn, "UPDATE videos SET alert_status = $status WHERE id = $idVideo");
            if($dataUpdate) $res = [ 'status' => 'success', 'message' => 'Cập nhật thành công', 'data' => $status];
            echo json_encode($res); 
        }else{
            $res = ['status' => 'error', 'message' => 'Cập nhật không thành công'];
            echo json_encode($res); 
        }
    }else{
        $res = ['status' => 'error', 'message' => 'Cập nhật không thành công, Sai dữ liệu đầu vào'];
        echo json_encode($res);  
    }