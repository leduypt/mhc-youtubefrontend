<?php
    include 'config.php';
    $conn = mysqli_connect($host, $username, $password , $database);
    $idVideo = '';
    if(isset($_GET["video"])) $idVideo = $_GET["video"];
    if($idVideo){
        $data = mysqli_query($conn,"SELECT * FROM videos WHERE id = $idVideo")->fetch_object();
        $res = [
            'status' => 'success',
            'data' => $data
        ];
        echo json_encode($res);
    }else{
        $res = [
            'status' => 'error',
            'data' => []
        ];
        echo json_encode($res); 
    }