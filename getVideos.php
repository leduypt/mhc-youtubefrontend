<?php
    include 'config.php';
    $conn = mysqli_connect($host, $username, $password , $database);
    $page = 1;
    $limit = 50;

    $query = "SELECT videos.parent, videos.comment_id, videos.id, videos.channel_id, videos.channel_name, videos.status, videos.title, videos.description, videos.publishedAt, videos.alert_status, videos.company_id, videos.comments, videos.tags, videos.video_id, videos.keyword, videos.data, companys.company_name, videos.company_id FROM videos, companys WHERE companys.id = videos.company_id AND videos.remove=0";
    if(isset($_POST["alert"]) && $_POST["alert"] != 2) $query .= " && alert_status=".$_POST['alert'];
    if(isset($_POST["parent"]) && $_POST["parent"]) $query .= " && parent='".$_POST['parent']."'";
    if(isset($_POST["type"]) && $_POST["type"] != 'all') $query .= " && type='".$_POST['type']."'";
    if(isset($_POST["status"]) && $_POST["status"] != 2) $query .= " && status=".$_POST['status'];
    if(isset($_POST["company"]) && $_POST["company"] != 'all') $query .= " && company_id=".$_POST['company']; 
    if(isset($_POST["settlement"]) && $_POST["settlement"] != 'all') $query .= " && settlement='".$_POST['settlement']."'";
    if(isset($_POST["fromDate"]) && isset($_POST["toDate"]) && $_POST["fromDate"] && $_POST["toDate"]) $query .= " && publishedAt BETWEEN '".$_POST["fromDate"]." 00:00:00' AND '".$_POST["toDate"]." 00:00:00'";
    if(isset($_POST["keyword"]) && $_POST["keyword"]) $query .= " && title LIKE '%".$_POST['keyword']."%'";
    if(isset($_POST["page"]) && $_POST["alert"]) $page = $_POST['page'];
    $query .= ' ORDER BY videos.publishedAt DESC';

    $result = $conn->query($query); 
    $total_record = mysqli_num_rows($result);
    $total_page=ceil($total_record/$limit);

    if($page < 1) $page=1;
    if($page > $total_page) $page = $total_page;
    $start = ($page-1) * $limit;
    $query .= " LIMIT $start,$limit";
    $data = $conn->query($query);

    $html = '';
    if($data):
        foreach($data as $row):
            $html .= '<tr>';
                $html .= '<td class="mhc-videoid">'.$row["id"].'</td>';
                if($row["type"] == 'post'):
                    $html .= '<td class="title-video"><a href="https://www.youtube.com/watch?v='.$row["video_id"].'" target="_blank">'.$row["title"].'</a></td>';
                else:
                    $html .= '<td class="title-video"><a href="https://www.youtube.com/watch?v='.$row["video_id"].'&lc='.$row["comment_id"].'" target="_blank">'.$row["title"].'</a></td>';
                endif;
                if($row["parent"]):
                    $html .= '<td><a href="https://www.youtube.com/watch?v='.$row["parent"].'" target="_blank">'.$row["parent"].'</a></td>';
                else:
                   $html .= '<td></td>'; 
                endif;
                $html .= '<td class="description">'.$row["description"].'</td>';
                // $html .= '<td>';
                // $html .= ' <div class="tags">';
                //         $tags = explode(',', $row["tags"]);
                //         foreach ($tags as $tag):
                //             $html .= '<span class="tag is-success is-lowercase">'.$tag.'</span>';
                //         endforeach;
                //     $html .= '</div>';
                // $html .= '</td>';
                $html .= '<td class="keyword">'.$row["keyword"].'</br><button class="button is-danger is-light is-small is-fullwidth">'.$row["company_name"].'</button></td>';
                $date = new DateTime($row["publishedAt"]);
                $html .= '<td>'.$date->format('H:i:s d/m/Y').'</td>';
                $html .= '<td class="action-btn">';
                        $alertStatus = $row["alert_status"] == '1' ? 'alert is-warning' : 'no-alert';
                        $disableAlert = $row["alert_status"] == '1' ? 'disabled' : '';
                    $html .= '<div class="buttons are-small mhc-action '.$status.'">';
                        $html .= '<div class="company_id hide">'.$row["company_id"].'</div>';
                        $html .= '<button class="button is-danger is-small is-rounded btn-remove"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>';
                        // $html .= '<a href="/detail/index.php?id='.$row["video_id"].'" class="button is-primary is-small is-rounded btn-view" target="_blank">View</a>';
                        $html .= '<a href="/video.php?id='.$row["video_id"].'" class="button is-primary is-small is-rounded btn-view" target="_blank">View Comments</a>';
                        $html .= '<button class="button is-rounded is-small alert-btn '.$alertStatus.'"'.$disableAlert.'><i class="fa fa-bell-o" aria-hidden="true"></i> Alert</button>';
                    $html .= '</div>';
                    $html .= '<div class="checkStatus">';
                        $html .= '<div class="control has-icons-left">';
                            $html .= '<div class="select is-small is-rounded">';
                                $html .= '<select>';
                                    $checked = $row["status"] == "0" ? "selected " : "";
                                    $notchecked = $row["status"] == "1" ? "selected " : "";

                                    $html .= '<option value="0" '.$checked.' >Đã check</option>';
                                    $html .= '<option value="1" '.$notchecked.' >Chưa check</option>';
                                $html .= '</select>';
                            $html .=  '</div>';
                            $html .= '<div class="icon is-small is-left">';
                                $html .= '<i class="fa fa-smile-o" aria-hidden="true"></i>';
                            $html .= '</div>';
                        $html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="settlement">';
                        $html .= '<div class="control has-icons-left">';
                            $html .= '<div class="select is-small is-rounded">';
                                $html .= '<select>';
                                    $none = $row["settlement"] == "none" ? "selected " : "";
                                    $positive = $row["settlement"] == "positive" ? "selected " : "";
                                    $negative = $row["settlement"] == "negative" ? "selected " : "";
                                    $neutral = $row["settlement"] == "neutral" ? "selected " : "";

                                    $html .= '<option value="none" '.$none.' >None</option>';
                                    $html .= '<option value="negative" '.$negative.' >Negative</option>';
                                    $html .= '<option value="positive" '.$positive.' >Positive</option>';
                                    $html .= '<option value="neutral" '.$neutral.' >Neutral</option>';
                                $html .= '</select>';
                            $html .=  '</div>';
                            $html .= '<div class="icon is-small is-left">';
                                $html .= '<i class="fa fa-smile-o" aria-hidden="true"></i>';
                            $html .= '</div>';
                        $html .= '</div>';
                    $html .= '</div>';
            $html .= ' </td>';
            $html .= '</tr>';
        endforeach;
    else:
        $html .= '<td colspan="8" class="center">Không có dữ liêụ</td>';
    endif;

    echo $html;