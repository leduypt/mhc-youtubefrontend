jQuery(document).ready(function ($) {
  updateSettlement();
  updateStatus();
  clickAlert();
  clickRemove();
  paginationClick();

  $.datepicker.setDefaults({
    dateFormat: "yy-mm-dd",
  });

  $("#from_date").datepicker();
  $("#to_date").datepicker();

  $("#modal-alert .delete").click(function () {
    $("#modal-alert").removeClass("is-active");
  });

  $("#modal-alert .close-btn").click(function () {
    $("#modal-alert").removeClass("is-active");
  });

  $("#modal-view .delete").click(function () {
    $("#modal-view").removeClass("is-active");
  });

  $("#modal-view .close-btn").click(function () {
    $("#modal-view").removeClass("is-active");
  });
  function paginationClick() {
    $(".pagination-list .pagination-link").click(function () {
      // Loading
      $("body").addClass("is-loading");
      setTimeout(function () {
        $("body").removeClass("is-loading");
      }, 1500);
      console.log("Pagination");
      var page = $(this).attr("page-number");
      var keyword = $("#keyword").val();
      var fromDate = $("#from_date").val();
      var toDate = $("#to_date").val();
      var statusAlert = $(".alert-filter .select select").val();
      var statusSettlement = $(".settlement-filter .select select").val();
      var companyID = $(".company-filter .select select").val();
      var status = $(".status-filter .select select").val();
      var type = $(".type-filter .select select").val();
      var parent_id = $(".parent_id").text();
      var that = this;
      $.ajax({
        type: "POST",
        url: "/getVideos.php",
        data: {
          keyword: keyword,
          fromDate: fromDate,
          toDate: toDate,
          alert: statusAlert,
          settlement: statusSettlement,
          page: page,
          company: companyID,
          status: status,
          type: type,
          parent: parent_id,
        },
        success: function (data) {
          $(".pagination-list .pagination-link").removeClass("is-current");
          $(that).addClass("is-current");
          $(".mhc-video-table .table tbody").empty();
          $(".mhc-video-container .table tbody").html(data);
          clickAlert();
          clickRemove();
          updateSettlement();
          updateStatus();
        },
      });
    });
  }

  $(".mhc-filter .btn-filter").click(function () {
    // Loading
    $("body").addClass("is-loading");
    setTimeout(function () {
      $("body").removeClass("is-loading");
    }, 3500);
    // Filer
    console.log("Filter");
    console.log(status);
    var keyword = $("#keyword").val();
    var fromDate = $("#from_date").val();
    var toDate = $("#to_date").val();
    var statusAlert = $(".alert-filter .select select").val();
    var companyID = $(".company-filter .select select").val();
    var statusSettlement = $(".settlement-filter .select select").val();
    var status = $(".status-filter .select select").val();
    var type = $(".type-filter .select select").val();
    var parent_id = $(".parent_id").text();
    console.log(statusAlert);
    $.ajax({
      type: "POST",
      url: "/getVideos.php",
      data: {
        keyword: keyword,
        fromDate: fromDate,
        toDate: toDate,
        alert: statusAlert,
        settlement: statusSettlement,
        company: companyID,
        status: status,
        type: type,
        parent: parent_id,
      },
      success: function (data) {
        $(".mhc-video-container .table tbody").empty();
        $(".mhc-video-container .table tbody").html(data);
        clickAlert();
        clickRemove();
        updateSettlement();
        updateStatus();
      },
    });

    $.ajax({
      type: "POST",
      url: "/getPagination.php",
      data: {
        keyword: keyword,
        fromDate: fromDate,
        toDate: toDate,
        alert: statusAlert,
        settlement: statusSettlement,
        page: 1,
        company: companyID,
        status: status,
        type: type,
        parent: parent_id,
      },
      success: function (data) {
        $(".pagination").empty();
        $(".pagination").html(data);
        clickAlert();
        clickRemove();
        updateSettlement();
        updateStatus();
        paginationClick();
      },
    });
  });

  function updateSettlement() {
    $(".settlement select").change(function () {
      // Loading
      // $("body").addClass("is-loading");
      // setTimeout(function () {
      //   $("body").removeClass("is-loading");
      // }, 1500);
      // Update Settlement
      var status = $(this).val();
      var videoId = $(this).closest("tr").find(".mhc-videoid").text();
      var that = this;
      $.ajax({
        type: "POST",
        url: "updateSettlement.php",
        data: {
          video: videoId,
          status: status,
        },
        success: function (data) {
          alert("Update Success");
          $(that).find("option:selected").removeAttr("selected");
          $(that)
            .find("option[value='" + status + "']")
            .attr("selected", true);
        },
      });
    });
  }

  function updateStatus() {
    $(".checkStatus select").change(function () {
      // Loading
      // $("body").addClass("is-loading");
      // setTimeout(function () {
      //   $("body").removeClass("is-loading");
      // }, 1500);
      // Update Settlement
      var status = $(this).val();
      var videoId = $(this).closest("tr").find(".mhc-videoid").text();
      var that = this;

      $.ajax({
        type: "POST",
        url: "/updateStatus.php",
        data: {
          video: videoId,
          status: status,
        },
        success: function (data) {
          res = JSON.parse(data);
          $(that).find("option").removeAttr("selected");
          $(that)
            .find("option[value='" + status + "']")
            .attr("selected", true);
        },
      });
    });
  }

  function clickAlert() {
    $(".mhc-video-container .mhc-action .alert-btn").click(function () {
      // Loading
      $("body").addClass("is-loading");
      setTimeout(function () {
        $("body").removeClass("is-loading");
      }, 1500);
      // Alert
      console.log("Alert");
      var videoTitle = $(this)
        .closest("tr")
        .find(".title-video")
        .find("a")
        .text();
      var videoUrl = $(this)
        .closest("tr")
        .find(".title-video")
        .find("a")
        .attr("href");
      var videoId = $(this).closest("tr").find(".mhc-videoid").text();
      var companyName = $(this)
        .closest("tr")
        .find(".keyword")
        .find("button")
        .text();
      var companyId = $(this).closest(".mhc-action").find(".company_id").text();
      var messageContent =
        "<b>Monitaz Youtube</b> - Quy khach hang " +
        companyName +
        " dang co tin nhay cam. Anh/Chi xin vui long xem chi tiet tai day: <a href='" +
        videoUrl +
        "'>" +
        videoTitle +
        "</a>";
      var that = this;
      $.ajax({
        type: "POST",
        url: "/sendTelegram.php",
        data: {
          message: messageContent,
          video: videoId,
          company: companyId,
        },
        success: function (data) {
          res = JSON.parse(data);
          if (res.status != "success") {
            alert("Alert Success");
          } else {
            alert(res.message);
          }
        },
      });

      $.ajax({
        type: "GET",
        url: "/updateAlertStatus.php?video=" + videoId + "&status=1",
        data: "",
        success: function (data) {
          res = JSON.parse(data);
          if (res.status != "success") {
            alert(res.message);
          } else {
            $(that).removeClass("no-alert");
            $(that).addClass("alert");
            $(that).addClass("is-warning");
            $(that).attr("disabled", true);
          }
        },
      });
    });
  }

  function clickRemove() {
    $(".mhc-video-container .mhc-action .btn-remove").click(function () {
      // Loading
      // $("body").addClass("is-loading");
      // setTimeout(function () {
      //   $("body").removeClass("is-loading");
      // }, 1500);
      // Alert
      var videoId = $(this).closest("tr").find(".mhc-videoid").text();
      var that = this;

      $.ajax({
        type: "POST",
        url: "/updateRemove.php",
        data: {
          video: videoId,
        },
        success: function (data) {
          res = JSON.parse(data);
          if (res.status != "success") {
            alert(res.message);
          } else {
            alert("Đã xoá thành công!");
            $(that).closest("tr").remove();
          }
        },
      });
    });
  }
});
